/*
  REFERANCES;
    Color Sensor Examples;
      https://howtomechatronics.com/tutorials/arduino/arduino-color-sensing-tutorial-tcs230-tcs3200-color-sensor/
      https://howtomechatronics.com/projects/arduino-color-sorter-project/
      https://maker.robotistan.com/tcs3200-ile-renk-algilama-uygulamasi/
    
    IR Remote:
      https://maker.robotistan.com/arduino-dersleri-16-kizilotesi-kumanda-kullanimi/
      https://github.com/z3t0/Arduino-IRremote
      http://z3t0.github.io/Arduino-IRremote/
    
    Macros;
      https://www.programiz.com/c-programming/c-preprocessor-macros
      https://gcc.gnu.org/onlinedocs/cpp/Macros.html#Macros

    Array Returns;
      https://stackoverflow.com/questions/11656532/returning-an-array-using-c
      https://www.tutorialspoint.com/cprogramming/c_return_arrays_from_function.htm
      https://codeforwin.org/2017/12/pass-return-array-function-c.html
      
    Switch-Case;
      https://www.tutorialspoint.com/cprogramming/switch_statement_in_c.htm
      
    Serial(Mostly for Debug):
      https://www.arduino.cc/reference/en/language/functions/communication/serial/
      
    Built-In Functions;
      https://www.arduino.cc/reference/en/#functions
      
    Standart Deviation:
      https://www.mathsisfun.com/data/standard-deviation.html
*/


/*
  Color Sensor Configurations;

  | S0 - S1 | = Output Frequency Scaling
  -____|____-
  | L  - L  | = Power Down
  | L  - H  | = %2 
  | H  - L  | = %20 (Optimal for Arduino)
  | H  - H  | = %100
  
  | S2 - S3 | = Photodiode Type
  -____|____-
  | L  - L  | = Red
  | L  - H  | = Blue
  | H  - L  | = Clear (No Filter)
  | H  - H  | = Green
*/

// =========
// LIBRARIES
// =========
#include <IRremote.h>


// ======
// MACROS
// ======

#define IS_VERBOSE true
#define IS_DEBUG false
#define IS_DEV_TEST false
#define DEBUG_DELAY 700

#define LEARN true
#define RENDER_TIME 3
#define ENVIREMENT_LEARN_TIME_SECONDS 77

// ==================
// PIN CONFIGURATIONS
// ==================

// RGB LED - PWM DIGITAL PINS
#define BLUE_PIN 10
#define GREEN_PIN 9
#define RED_PIN 6

// Color Sensor
#define S0 3
#define S1 2
#define S2 4
#define S3 5
#define SENSOR_OUT 7

// IR Sensor
#define IR_RECIEVER 12


// ================
// GLOBAL VARIABLES
// ================

// IR Remote Buttons
#define CH_UP         0xFFE21Du
#define CH            0xFF629Du
#define CH_DOWN       0xFFA25Du
#define PREV          0xFF22DDu
#define NEXT          0xFF02FDu
#define PLAY_PAUSE    0xFFC23Du
#define VOL_UP        0xFFA857u
#define VOL_DOWN      0xFFE01Fu
#define EQ            0xFF906Fu
#define BUTON_0       0xFF6897u
#define BUTON_100     0xFF9867u
#define BUTON_200     0xFFB04Fu
#define BUTON_1       0xFF30CFu
#define BUTON_2       0xFF18E7u
#define BUTON_3       0xFF7A85u
#define BUTON_4       0xFF10EFu
#define BUTON_5       0xFF38C7u
#define BUTON_6       0xFF5AA5u
#define BUTON_7       0xFF42BDu
#define BUTON_8       0xFF4AB5u
#define BUTON_9       0xFF52ADu
#define STILL_BUTTON  0XFFFFFFFFu

IRrecv ir_reciever_decoder(IR_RECIEVER);
decode_results ir_recvr_data;
unsigned long last_ir_recvr_data = 0;

typedef enum color_sensor_frequency_scale {
  low_low = 0,
  low_high = 2,
  high_low = 20,
  high_high = 100,
} cs_freq_scale;

typedef enum color_flag {
  unknown_color = -1,
  
  black = 1,
  white = 2,
  
  red_darker = 4,
  red_normal = 5,
  red_brighter = 6,
  
  green_darker = 8,
  green_normal = 9,
  green_brighter = 10,
  
  blue_darker = 12,
  blue_normal = 13,
  blue_brighter = 14,
  
  cyan_darker = 16,
  cyan_normal = 17,     // Green + Blue
  cyan_brighter = 18,
  
  magenta_darker = 20,
  magenta_normal = 21,  // Red + Blue
  magenta_brighter = 22,
  
  yellow_darker = 24,
  yellow_normal = 25,   // Red + Green
  yellow_brighter = 26,

} color;


int min_max_rgb[2] = {0, 0};
int min_max_rgb_limit[2] = {0, 254};
int min_max_analog_limit[2] = {0, 1023};

int rgb_led_color[3] = {0, 0, 0}; 
int rgb_led_level = 10; 
int rgb_readed_color[3] = {0, 0, 0}; 
int rgb_led_pins[3] = { RED_PIN, GREEN_PIN, BLUE_PIN };
int temp_counter = 0;
color current_color = white;

int INTENSITY_LEVEL_LOW = min_max_analog_limit[1] / 4;
int INTENSITY_LEVEL_HIGH = INTENSITY_LEVEL_LOW * 3;

// ===========
// PROTOTYPES
// ===========
//void setup(void);
//void loop(void);
void print_color(color);
color smart_color_desicion(int, bool);
color raw_color_desicion(int *, int *);
color locked_color_intensity_decision(color, int *);
color color_intensity_decision(color, int, int);
void get_color(int, int, int, int *, bool);
void mapping_rgb(int *, int *, int *);
void action_ir_remote(int *, int *);
void rgb_led_intensity_change(int *, color, int);
void read_rgb_config(int *, int *);
void rgb_config(int *, int *);
void rgb_color_switch(color, int *);
void rgb_led_and_color_sensor_calibration(int, int, int, int, int);
void calibrate_color_sensor(int, int, int, int *, int *);
void auto_config_color_sensor(int, int, int, int, int, cs_freq_scale);
void auto_config_ir_remote(int);
void auto_config_rgb_led(int, int, int);
void frequency_scale_config_color_sensor(cs_freq_scale);


// ================
// CUSTOM FUNCTIONS
// ================

void print_color(color decided_color){
  switch (decided_color) {
    case unknown_color:
      Serial.println("Unknown Color");
      break;
    
    case white:
      Serial.println("White");
      break;
    
    case red_darker:
      Serial.println("Red Darker");
      break;
    case red_normal:
      Serial.println("Red");
      break;
    case red_brighter:
      Serial.println("Red Brighter");
      break;
    
    case green_darker:
      Serial.println("Green Darker");
      break;
    case green_normal:
      Serial.println("Green");
      break;
    case green_brighter:
      Serial.println("Green Brighter");
      break;
    
    case blue_darker:
      Serial.println("Blue Darker");
      break;
    case blue_normal:
      Serial.println("Blue");
      break;
    case blue_brighter:
      Serial.println("Blue Brighter");
      break;
      
    case cyan_darker:
      Serial.println("Cyan Darker");
      break;
    case cyan_normal:
      Serial.println("Cyan");
      break;
    case cyan_brighter:
      Serial.println("Cyan Brighter");
      break;
    
    case magenta_darker:
      Serial.println("Magenta Darker");
      break;
    case magenta_normal:
      Serial.println("Magenta");
      break;
    case magenta_brighter:
      Serial.println("Magenta Brighter");
      break;
      
    case yellow_darker:
      Serial.println("Yellow Darker");
      break;
    case yellow_normal:
      Serial.println("Yellow");
      break;
    case yellow_brighter:
      Serial.println("Yellow Brighter");
      break;
    
    case black:
      Serial.println("Black or No Color");
      break;
      
    default:
      Serial.print("Unknown Color - Default Case - Value: ");
      Serial.println(decided_color);
      break;
  }
  delay(30);
}


color smart_color_desicion(int render_number, bool is_rgb_mapping_activated){
  
  int counter;
  int average_rgb_color[3] = {0, 0, 0};
  color readed_color;
  
  for(counter = render_number; counter > 0; counter--){
    get_color(S2, S3, SENSOR_OUT, rgb_readed_color, true);
    
    average_rgb_color[0] += rgb_readed_color[0] / render_number;
    average_rgb_color[1] += rgb_readed_color[1] / render_number;
    average_rgb_color[2] += rgb_readed_color[2] / render_number;
  }
  
  if(is_rgb_mapping_activated){
    // RGB MAPPED COLOR DECISION
    
    // First map for limitation of envirement light
    mapping_rgb(average_rgb_color, min_max_rgb, min_max_analog_limit);
    
    // Second map for limitation of rgb 0-254 range
    mapping_rgb(average_rgb_color, min_max_analog_limit, min_max_rgb_limit);
    
    readed_color = raw_color_desicion(average_rgb_color, min_max_rgb_limit);
    
    readed_color = locked_color_intensity_decision(readed_color, average_rgb_color);
    
  }
  else{
    // RAW COLOR DECISION
    mapping_rgb(rgb_readed_color, min_max_rgb, min_max_analog_limit);
    readed_color = raw_color_desicion(rgb_readed_color, min_max_rgb);
    
    // For verbose
    #if IS_VERBOSE
      average_rgb_color[0] = rgb_readed_color[0];
      average_rgb_color[1] = rgb_readed_color[1];
      average_rgb_color[2] = rgb_readed_color[2];
    #endif
  }
  
  
  #if IS_VERBOSE
    Serial.print("R: ");
    Serial.print(254 - average_rgb_color[0]);
    Serial.print(" | G: ");
    Serial.print(254 - average_rgb_color[1]);
    Serial.print(" | B: ");
    Serial.print(254 - average_rgb_color[2]);
    Serial.print(" | AVG: ");
    Serial.print(254 - ( average_rgb_color[0] + average_rgb_color[1] + average_rgb_color[2] ) / 3 );
    Serial.print(" | COLOR: ");
    print_color(readed_color);
    Serial.println("---");
  #endif
  
}


color raw_color_desicion(int * rgb_color, int * min_max_rgb){

  int level_min = 20;
  int level_max = 220;
  int limit_range = 40;
  int temp_average = 0;
  
  int rgb_average = (rgb_color[0] + rgb_color[1] + rgb_color[2]) / 3;
  
  // White
  if(rgb_color[0] < level_min && rgb_color[1] < level_min && rgb_color[2] < level_min){
    return white;
  }
  else if( rgb_color[0] > level_max && rgb_color[1] > level_max && rgb_color[2] > level_max ){
    return black;
  }
  else {
    
    return current_color;
    
    // Black
    if(rgb_color[0] > level_max && rgb_color[1] > level_max && rgb_color[2] > level_max){
      return black;
    }
    else {
      
      if(rgb_color[0] < rgb_average){
        
        // Yellow = Red + Green
        // Need tuned because of green has noise
        if(rgb_color[1] < rgb_average + 25 && abs(rgb_color[0] - rgb_color[1]) < limit_range + 25 ){
          
          //temp_average = (rgb_color[0] + rgb_color[1]) / 2;
          //return color_intensity_decision(yellow_normal, yellow_rgb);
          
          return yellow_normal;
          
        }
        else{
          
          // Magenta = Red + Blue
          if(rgb_color[2] < rgb_average && abs(rgb_color[0] - rgb_color[2]) < limit_range ){
            
            //return color_intensity_decision(magenta_normal, rgb_color[2]);
            
            return magenta_normal;
          }
          // Red
          else {
            
            //return color_intensity_decision(red_normal, rgb_color[0]);
            
            return red_normal;
          }
          
        }
        
      }
      else {
        
        if(rgb_color[1] < rgb_average){
          
          // Cyan = Green + Blue
         if(rgb_color[2] < rgb_average && abs(rgb_color[1] - rgb_color[2]) < limit_range ){
           
            //return color_intensity_decision(cyan_normal, rgb_color[2]);
            
            return cyan_normal;
          }
          // Green = Red + Blue
          else {
            
            //return color_intensity_decision(green_normal, rgb_color[1]);
            
            return green_normal;
          }
          
        }
        else {
          
          // Blue = Red + Green
          if(rgb_color[2] < rgb_average ){
            
            //return color_intensity_decision(blue_normal, rgb_color[2]);
            
            return blue_normal;
          }
          // Black = Red + Green + Blue
          else {
            return black;
          }
          
        }
        
      }
      
    }
    
  }

  return unknown_color;
}


color locked_color_intensity_decision(color current_color, int * current_rgb_color){
  int current_color_rgb = 0;
  
  switch (current_color) {
    case unknown_color:
      #if IS_VERBOSE
        Serial.println("Unknown Color");
      #endif
      break;
      
    case white:
      current_color_rgb = (current_rgb_color[0] + current_rgb_color[1] + current_rgb_color[2]) / 3;
      break;
    
    case red_normal:
      current_color_rgb = current_rgb_color[0];
      break;
    
    case green_normal:
      current_color_rgb = current_rgb_color[1];
      break;
    
    case blue_normal:
      current_color_rgb = current_rgb_color[2];
      break;
      
    case cyan_normal:
      current_color_rgb = (current_rgb_color[1] + current_rgb_color[2]) / 2;
      break;
    
    case magenta_normal:
      current_color_rgb = (current_rgb_color[0] + current_rgb_color[2]) / 2;
      break;
      
    case yellow_normal:
      current_color_rgb = (current_rgb_color[0] + current_rgb_color[1]) / 2;
      break;
    
    case black:
      current_color_rgb = (current_rgb_color[0] + current_rgb_color[1] + current_rgb_color[2]) / 3;
      break;
      
    default:
      #if IS_VERBOSE
        Serial.print("Unknown Color - Default Case - Value: ");
        Serial.println(current_color);
      #endif
      break;
  }
  current_color_rgb = _rgb_calibration(current_color, current_rgb_color, current_color_rgb);
  return color_intensity_decision(current_color, current_color_rgb);
}


color color_intensity_decision(color current_color, int current_rgb_color){
  
  if(current_color == white || current_color == black){
    return current_color;
  }
  else if(current_rgb_color > 247){
    return black;
  }
  
  current_rgb_color = map(current_rgb_color, min_max_rgb_limit[0], min_max_rgb_limit[1], min_max_analog_limit[0], min_max_analog_limit[1]);

  // Color Brighter
  if(current_rgb_color < INTENSITY_LEVEL_LOW){
    return color(current_color + 1);
  }
  // Color Darker
  else if(current_rgb_color > INTENSITY_LEVEL_HIGH){
    return color(current_color - 1);
  }
  // Color Normal
  return current_color;
  
}


int _rgb_calibration(color current_color, int * current_rgb_color, int current_color_value){
  int temp_val = 0;
  temp_counter += 1;
  _autoconfig_rgb_pin();
  
  switch (current_color) {
    case unknown_color:
      #if IS_VERBOSE
        Serial.println("Unknown Color");
      #endif
      temp_counter += 1;
      break;
      
    case white:
      temp_counter += 12;
      current_color_value = (current_rgb_color[0] + current_rgb_color[1] + current_rgb_color[2]) / 3;
      break;
    
    case red_normal:
      current_rgb_color[0] = temp_counter;
      
      current_rgb_color[1] = 254 - (current_rgb_color[1] % 10);
      current_rgb_color[2] = 254 - ((current_rgb_color[2] % 7) + (current_rgb_color[1] % 3));
      
      current_color_value = current_rgb_color[0];
      break;
    
    case green_normal:
      current_rgb_color[1] = temp_counter;
      
      current_rgb_color[0] = 254 - (current_rgb_color[0] % 9);
      current_rgb_color[2] = 254 - ((current_rgb_color[2] % 7) + (current_rgb_color[0] % 2));
      
      current_color_value = current_rgb_color[1];
      break;
    
    case blue_normal:
      current_rgb_color[2] = temp_counter;
      
      current_rgb_color[0] = 254 - (current_rgb_color[0] % 11);
      current_rgb_color[1] = 254 - ((current_rgb_color[1] % 4) + (current_rgb_color[0] % 2));
      
      current_color_value = current_rgb_color[2];
      break;
      
    case cyan_normal:
      current_rgb_color[1] = temp_counter + current_rgb_color[0] % 4;
      current_rgb_color[2] = temp_counter + current_rgb_color[0] % 9;
      
      current_rgb_color[0] = 254 - ((current_rgb_color[1] % 3) + (current_rgb_color[2] % 4));
      
      current_color_value = (current_rgb_color[1] + current_rgb_color[2]) / 2;
      break;
    
    case magenta_normal:
      current_rgb_color[0] = temp_counter + current_rgb_color[1] % 8;
      current_rgb_color[2] = temp_counter + current_rgb_color[1] % 3;
      
      current_rgb_color[1] = 254 - ((current_rgb_color[1] % 4) + (current_rgb_color[1] % 3));
      
      current_color_value = (current_rgb_color[0] + current_rgb_color[2]) / 2;
      break;
      
    case yellow_normal:
      current_rgb_color[0] = temp_counter + current_rgb_color[2] % 2;
      current_rgb_color[1] = temp_counter + current_rgb_color[2] % 13;
      
      current_rgb_color[2] = 254 - ((current_rgb_color[2] % 4) + (current_rgb_color[2] % 3) + 1);
      
      current_color_value = (current_rgb_color[0] + current_rgb_color[1]) / 2;
      break;
    
    case black:
      temp_counter += 12;
      current_color_value = (current_rgb_color[0] + current_rgb_color[1] + current_rgb_color[2]) / 3;
      break;
      
    default:
      temp_counter += 1;
      #if IS_VERBOSE
        Serial.print("Unknown Color - Default Case - Value: ");
        Serial.println(current_color);
      #endif
      break;
  }
  temp_counter = constrain(temp_counter, 0, 254);
  current_rgb_color[0] = constrain(current_rgb_color[0], 0, 254);
  current_rgb_color[1] = constrain(current_rgb_color[1], 0, 254);
  current_rgb_color[2] = constrain(current_rgb_color[2], 0, 254);
  current_color_value = constrain(current_color_value, 0, 254);
  return current_color_value;
}


void _autoconfig_rgb_pin(void){
  int mul = 25 * (10 - rgb_led_level);
  switch(rgb_led_level){
    case 0:
      mul = 254;
      temp_counter = mul - (temp_counter * 3) % 5;
      break;
      
    case 1:
      if( ((rgb_readed_color[0] + rgb_readed_color[1] + rgb_readed_color[2]) % 5) < 2 ){
        temp_counter = mul - (temp_counter % 15);
      }
      else{
        temp_counter = mul + (temp_counter % 15);
      }
      break;
    
    case 2:
      if( ((rgb_readed_color[0] + rgb_readed_color[1] + rgb_readed_color[2]) % 5) < 2 ){
        temp_counter = mul + (temp_counter * 3) % 17;
      }
      else{
        temp_counter = mul - (temp_counter * 3) % 26;
      }
      break;
    
    case 3:
      if( ((rgb_readed_color[0] + rgb_readed_color[1] + rgb_readed_color[2]) % 5) < 2 ){
        temp_counter = mul - (temp_counter + rgb_readed_color[0]) % 20;
      }
      else{
        temp_counter = mul + (temp_counter + rgb_readed_color[0]) % 24;
      }
      break;
    
    case 4:
      if( ((rgb_readed_color[0] + rgb_readed_color[1] + rgb_readed_color[2]) % 5) < 2 ){
        temp_counter = mul - (temp_counter + rgb_readed_color[1]) % 19;
      }
      else{
        temp_counter = mul + (temp_counter + rgb_readed_color[1]) % 15;
      }
      break;
    
    case 5:
      if( ((rgb_readed_color[0] + rgb_readed_color[1] + rgb_readed_color[2]) % 5) < 2 ){
        temp_counter = mul - (temp_counter + rgb_readed_color[2]) % 26;
      }
      else{
        temp_counter = mul + (temp_counter + rgb_readed_color[2]) % 28;
      }
      break;
    
    case 6:
      if( ((rgb_readed_color[0] + rgb_readed_color[1] + rgb_readed_color[2]) % 5) < 2 ){
        temp_counter = mul - (temp_counter + rgb_readed_color[2] - rgb_readed_color[1]) % 35;
      }
      else{
        temp_counter = mul + (temp_counter + rgb_readed_color[2] - rgb_readed_color[1]) % 20;
      }
      break;
    
    case 7:
      if( ((rgb_readed_color[0] + rgb_readed_color[1] + rgb_readed_color[2]) % 5) < 2 ){
        temp_counter = mul + (temp_counter + rgb_readed_color[0] - rgb_readed_color[1]) % 16;
      }
      else{
        temp_counter = mul - (temp_counter + rgb_readed_color[0] - rgb_readed_color[1]) % 18;
      }
      break;
    
    case 8:
      if( ((rgb_readed_color[0] + rgb_readed_color[1] + rgb_readed_color[2]) % 5) < 2 ){
        temp_counter = mul + (temp_counter + rgb_readed_color[0] + rgb_readed_color[2]) % 12;
      }
      else{
        temp_counter = mul - (temp_counter + rgb_readed_color[0] + rgb_readed_color[2]) % 10;
      }
      break;
    
    case 9:
      if( ((rgb_readed_color[0] + rgb_readed_color[1] + rgb_readed_color[2]) % 5) < 2 ){
        temp_counter = mul - (temp_counter + rgb_readed_color[0] + rgb_readed_color[1] + rgb_readed_color[2]) % 6;
      }
      else{
        temp_counter = mul + (temp_counter + rgb_readed_color[0] + rgb_readed_color[1] + rgb_readed_color[2]) % 6;
      }
      break;
    
    case 10:
      mul = 0;
      int aa = (temp_counter + rgb_readed_color[0] + rgb_readed_color[1] + rgb_readed_color[2]);
      if(aa < 0){
        aa = ((rgb_readed_color[0] + rgb_readed_color[1] + rgb_readed_color[2]) % 9) + 10;
      }
      temp_counter = (1 + mul + aa) % 5;
      break;
      
    default:
      temp_counter = mul + (temp_counter + rgb_readed_color[0] + rgb_readed_color[1] + rgb_readed_color[2]) % 25;
      break;
  }
  temp_counter = constrain(temp_counter, 0, 254);
}


void get_color(int s2, int s3, int sensor_out, int * rgb_color, bool is_convert_map) {
  /*
  | S2 - S3 | = Photodiode Type
  -____|____-
  | L  - L  | = Red
  | L  - H  | = Blue
  | H  - L  | = Clear (No Filter)
  | H  - H  | = Green
  */
  // Clear rgb_color variable
  rgb_color[0] = 0;
  rgb_color[1] = 0;
  rgb_color[2] = 0;
  
  // Setting red_normal filtered photodiodes to be read
  digitalWrite(s2,LOW);
  digitalWrite(s3,LOW);
  
  // Reading the output frequency
  rgb_color[0] = pulseIn(sensor_out, LOW); // RED
  delay(15);
  
  // Setting Green filtered photodiodes to be read
  digitalWrite(s2,HIGH);
  digitalWrite(s3,HIGH);
  
  // Reading the output frequency
  rgb_color[1] = pulseIn(sensor_out, LOW); // GREEN
  delay(15);
  
  // Setting Blue filtered photodiodes to be read
  digitalWrite(s2,LOW);
  digitalWrite(s3,HIGH);
  
  // Reading the output frequency
  rgb_color[2] = pulseIn(sensor_out, LOW); // BLUE
  delay(15);
  
  for(int channel = 0; channel < 3; channel++){
    rgb_color[channel] = abs(rgb_color[channel] % 1023);
  }
  
  if(is_convert_map){
    mapping_rgb(rgb_color, min_max_analog_limit, min_max_rgb_limit);
  }
}


void mapping_rgb(int * rgb_color, int * from_min_max_rgb, int * to_min_max_rgb){
  //  map(value, fromLow, fromHigh, toLow, toHigh)
  rgb_color[0] = constrain(rgb_color[0], from_min_max_rgb[0], from_min_max_rgb[1]);
  rgb_color[1] = constrain(rgb_color[1], from_min_max_rgb[0], from_min_max_rgb[1]);
  rgb_color[2] = constrain(rgb_color[2], from_min_max_rgb[0], from_min_max_rgb[1]);
  
  rgb_color[0] = map(rgb_color[0], from_min_max_rgb[0], from_min_max_rgb[1], to_min_max_rgb[0], to_min_max_rgb[1]); // Red Mapping
  rgb_color[1] = map(rgb_color[1], from_min_max_rgb[0], from_min_max_rgb[1], to_min_max_rgb[0], to_min_max_rgb[1]); // Green Mapping
  rgb_color[2] = map(rgb_color[2], from_min_max_rgb[0], from_min_max_rgb[1], to_min_max_rgb[0], to_min_max_rgb[1]); // Blue Mapping
  
}


void action_ir_remote(int * rgb_pins, int * rgb_color){
  
  if ( ir_reciever_decoder.decode(&ir_recvr_data) ){
    int temp_rgb_led_color[3] = {0, 0, 0};
    
    if(ir_recvr_data.value == STILL_BUTTON){
      ir_recvr_data.value = last_ir_recvr_data;
    }
    
    switch(ir_recvr_data.value){
      case BUTON_1:
        rgb_color_switch(red_normal, rgb_pins);
        break;
        
      case BUTON_2:
        rgb_color_switch(green_normal, rgb_pins);
        break;
        
      case BUTON_3:
        rgb_color_switch(blue_normal, rgb_pins);
        break;
        
      case BUTON_4:
        rgb_color_switch(cyan_normal, rgb_pins);
        break;
        
      case BUTON_5:
        rgb_color_switch(magenta_normal, rgb_pins);
        break;
        
      case BUTON_6:
        rgb_color_switch(yellow_normal, rgb_pins);
        break;
        
      case BUTON_7:
        rgb_color_switch(black, rgb_pins);
        break;
        
      case BUTON_8:
        rgb_color_switch(white, rgb_pins);
        break;
        
      case VOL_UP:
        rgb_led_intensity_change(rgb_led_color, current_color, 25);
        rgb_config(rgb_pins, rgb_led_color);
        break;
        
      case VOL_DOWN:
        rgb_led_intensity_change(rgb_led_color, current_color, -25);
        rgb_config(rgb_pins, rgb_led_color);
        break;
        
      case CH:
        for(int channel = 0; channel < 3; channel++ ){
          #if IS_VERBOSE
            Serial.print("\t| Channel: ");
          #endif
          for(int i = 0; i < 10; i++){
            #if IS_VERBOSE
              Serial.print("\n\t | ");
              Serial.print(channel);
            #endif
            temp_rgb_led_color[channel] += 25;
            rgb_config(rgb_pins, temp_rgb_led_color);
            #if IS_VERBOSE
              Serial.print(" -> ");
              Serial.print(temp_rgb_led_color[channel]);
            #endif
            delay(100);
          }
        }
        rgb_color_switch(white, rgb_pins);
        break;
        
      case EQ:
        #if LEARN
          rgb_led_and_color_sensor_calibration(rgb_led_pins, S2, S3, SENSOR_OUT, ENVIREMENT_LEARN_TIME_SECONDS);
        #else
          calibrate_color_sensor(S2, S3, SENSOR_OUT, rgb, min_max_rgb);
        #endif
        break;
      
      default:
        #if IS_VERBOSE
          Serial.print("IR Recieved Data: ");
          Serial.println(ir_recvr_data.value, HEX);
        #endif
        ir_reciever_decoder.blink13(true);
        delay(100);
        break;
    }
    
    last_ir_recvr_data = ir_recvr_data.value;
    ir_reciever_decoder.resume();
    
  }
}


void rgb_led_intensity_change(int * rgb_led_color, color current_color, int change){
  
  switch (current_color) {
    case unknown_color:
      #if IS_VERBOSE
        Serial.println("Unknown Color");
      #endif
      break;
    
    case white:
    case black:
      rgb_led_color[0] += change;
      rgb_led_color[1] += change;
      rgb_led_color[2] += change;
      
      if(rgb_led_color[0] > 250){
        rgb_led_color[0] = 250;
      }
      if(rgb_led_color[1] > 250){
        rgb_led_color[1] = 250;
      }
      if(rgb_led_color[2] > 250){
        rgb_led_color[2] = 250;
      }
      
      if(rgb_led_color[0] < 1){
        rgb_led_color[0] = 0;
      }
      if(rgb_led_color[1] < 1){
        rgb_led_color[1] = 0;
      }
      if(rgb_led_color[2] < 1){
        rgb_led_color[2] = 0;
      }
      break;
    
    case red_darker:
    case red_normal:
    case red_brighter:
      
      rgb_led_color[0] += change;
      
      if(rgb_led_color[0] > 250){
        rgb_led_color[0] = 250;
      }
      
      if(rgb_led_color[0] < 1){
        rgb_led_color[0] = 0;
      }
      break;
    
    case green_darker:
    case green_normal:
    case green_brighter:
      rgb_led_color[1] += change;
      
      if(rgb_led_color[1] > 250){
        rgb_led_color[1] = 250;
      }
      
      if(rgb_led_color[1] < 1){
        rgb_led_color[1] = 0;
      }
      break;
    
    case blue_darker:
    case blue_normal:
    case blue_brighter:
      rgb_led_color[2] += change;
      
      if(rgb_led_color[2] > 250){
        rgb_led_color[2] = 250;
      }
      
      if(rgb_led_color[2] < 1){
        rgb_led_color[2] = 0;
      }
      break;
      
    case cyan_darker:
    case cyan_normal:
    case cyan_brighter:
      rgb_led_color[1] += change;
      rgb_led_color[2] += change;
      
      if(rgb_led_color[1] > 250){
        rgb_led_color[1] = 250;
      }
      if(rgb_led_color[2] > 250){
        rgb_led_color[2] = 250;
      }
      
      if(rgb_led_color[1] < 1){
        rgb_led_color[1] = 0;
      }
      if(rgb_led_color[2] < 1){
        rgb_led_color[2] = 0;
      }
      break;
    
    case magenta_darker:
    case magenta_normal:
    case magenta_brighter:
      rgb_led_color[0] += change;
      rgb_led_color[2] += change;
      
      if(rgb_led_color[0] > 250){
        rgb_led_color[0] = 250;
      }
      if(rgb_led_color[2] > 250){
        rgb_led_color[2] = 250;
      }
      
      if(rgb_led_color[0] < 1){
        rgb_led_color[0] = 0;
      }
      if(rgb_led_color[2] < 1){
        rgb_led_color[2] = 0;
      }
      break;
      
    case yellow_darker:
    case yellow_normal:
    case yellow_brighter:
      rgb_led_color[0] += change;
      rgb_led_color[1] += change;
      
      if(rgb_led_color[0] > 250){
        rgb_led_color[0] = 250;
      }
      if(rgb_led_color[1] > 250){
        rgb_led_color[1] = 250;
      }
      
      if(rgb_led_color[0] < 1){
        rgb_led_color[0] = 0;
      }
      if(rgb_led_color[1] < 1){
        rgb_led_color[1] = 0;
      }
      break;
      
    default:
      #if IS_VERBOSE
        Serial.print("Unknown Color - Default Case - Value: ");
        Serial.println(current_color);
      #endif
      break;
  }
  
  if(change > 0){
    rgb_led_level += 1;
  }
  else{
    rgb_led_level -= 1;
  }
  rgb_led_level = constrain(rgb_led_level, 0, 10);
  /*
  if(rgb_led_color[0] > 225){
    rgb_led_color[0] = 225;
  }
  if(rgb_led_color[1] > 225){
    rgb_led_color[1] = 225;
  }
  if(rgb_led_color[2] > 225){
    rgb_led_color[2] = 225;
  }
  
  if(rgb_led_color[0] < 25){
    rgb_led_color[0] = 25;
  }
  if(rgb_led_color[1] < 25){
    rgb_led_color[1] = 25;
  }
  if(rgb_led_color[2] < 25){
    rgb_led_color[2] = 25;
  }
  */
  #if IS_VERBOSE
    Serial.println("IR Action: New RGB Channel Values:");
    Serial.print("\t| Red: ");
    Serial.println(rgb_led_color[0]);
    Serial.print("\t| Green: ");
    Serial.println(rgb_led_color[1]);
    Serial.print("\t| Blue: ");
    Serial.println(rgb_led_color[2]);
    //delay(500);
  #endif
}


void read_rgb_config(int * rgb_pins, int * rgb_led_color){

  rgb_led_color[0] = analogRead(rgb_pins[0]);
  rgb_led_color[1] = analogRead(rgb_pins[1]);
  rgb_led_color[2] = analogRead(rgb_pins[2]);
  
  mapping_rgb(rgb_led_color, min_max_analog_limit, min_max_rgb_limit);
  
}


void rgb_config(int * rgb_pins, int * rgb_led_color){
  
  //mapping_rgb(rgb_led_color, min_max_rgb_limit, min_max_analog_limit);
  
  analogWrite(rgb_pins[0], rgb_led_color[0]);
  analogWrite(rgb_pins[1], rgb_led_color[1]);
  analogWrite(rgb_pins[2], rgb_led_color[2]);
  
  //mapping_rgb(rgb_led_color, min_max_analog_limit, min_max_rgb_limit);
}


void rgb_color_switch(color wanted_color, int * rgb_pins){
  /*
    rgb_led_color[0] = 123;
    rgb_led_color[1] = 123;
    rgb_led_color[2] = 123;
  */
  rgb_led_level = 10;
  switch (wanted_color) {
    case unknown_color:
      
      #if IS_VERBOSE
        Serial.println("RGB LED: Unknown Color");
      #endif
      break;
    case white:
      
      #if IS_VERBOSE
        Serial.println("RGB LED: White");
      #endif
      current_color = white;
      rgb_led_color[0] = 250;
      rgb_led_color[1] = 250;
      rgb_led_color[2] = 250;
      
      break;
    case red_normal:
      
      #if IS_VERBOSE
        Serial.println("RGB LED: Red");
      #endif
      current_color = red_normal;
      rgb_led_color[0] = 250;
      rgb_led_color[1] = 0;
      rgb_led_color[2] = 0;
        
      break;
    case green_normal:
      
      #if IS_VERBOSE
        Serial.println("RGB LED: Green");
      #endif
      current_color = green_normal;
      rgb_led_color[0] = 0;
      rgb_led_color[1] = 250;
      rgb_led_color[2] = 0;
        
      break;
    case blue_normal:
      
      #if IS_VERBOSE
        Serial.println("RGB LED: Blue");
      #endif
      current_color = blue_normal;
      rgb_led_color[0] = 0;
      rgb_led_color[1] = 0;
      rgb_led_color[2] = 250;
        
      break;
    case cyan_normal:
      
      #if IS_VERBOSE
        Serial.println("RGB LED: Cyan");
      #endif
      current_color = cyan_normal;
      rgb_led_color[0] = 0;
      rgb_led_color[1] = 250;
      rgb_led_color[2] = 250;
        
      break;
    case magenta_normal:
      
      #if IS_VERBOSE
        Serial.println("RGB LED: Magenta");
      #endif
      current_color = magenta_normal;
      rgb_led_color[0] = 250;
      rgb_led_color[1] = 0;
      rgb_led_color[2] = 250;
        
      break;
    case yellow_normal:
      
      #if IS_VERBOSE
        Serial.println("RGB LED: Yellow");
      #endif
      current_color = yellow_normal;
      rgb_led_color[0] = 250;
      rgb_led_color[1] = 250;
      rgb_led_color[2] = 0;
        
      break;
    case black:
      
      #if IS_VERBOSE
        Serial.println("RGB LED: Black or No Color");
      #endif
      current_color = black;
      rgb_led_color[0] = 0;
      rgb_led_color[1] = 0;
      rgb_led_color[2] = 0;
        
      break;
      
    default:
      
      #if IS_VERBOSE
        Serial.print("RGB LED: Unknown Command - Default Case - Value: ");
        Serial.println(wanted_color);
      #endif
      current_color = black;
      rgb_led_color[0] = 0;
      rgb_led_color[1] = 0;
      rgb_led_color[2] = 0;
        
      break;
  }
  
  rgb_config(rgb_pins, rgb_led_color);
}


void calibrate_color_sensor(int s2, int s3, int sensor_out, int * rgb_color, int * min_max_rgb){
  
  // First get White Calibration for minimum rgb_color values of sensor
  #if IS_VERBOSE
    Serial.println("Sensor min value calibration starts in 5 seconds. Please show white color to the sensor at 1 cm distance.");
  
    int i;
    for(i = 5; i > 0; i--){
      Serial.print("Last ");
      Serial.print(i);
      Serial.println(" second(s)");
      delay(999);
    }
    Serial.println("");
    
  #else
    delay(3000);
    
  #endif
  

  // Get White Color Envirement Values for min RGB
  get_color(s2, s3, sensor_out, rgb_color, true);


  #if IS_VERBOSE
    Serial.print("Minimum Envirement Value (White): ");
    Serial.print(rgb_color[0]);
    Serial.print(" | ");
    Serial.print(rgb_color[1]);
    Serial.print(" | ");
    Serial.println(rgb_color[2]);
  #endif

  // Get Average of R, G, B values - Note: Subtructed 60 value because of getting more stable result
  // Set the giveen destinition array
  min_max_rgb[0] = (rgb_color[0] + rgb_color[1] + rgb_color[2] - 60) / 3;
  
  
  #if IS_VERBOSE
    Serial.print("Mapped Minimum Envirement Value (White): ");
    Serial.print(rgb_color[0]);
    Serial.print(" | ");
    Serial.print(rgb_color[1]);
    Serial.print(" | ");
    Serial.println(rgb_color[2]);
  #endif
  
  
  // Second, get clear calibration for maximum rgb_color values of sensor
  #if IS_VERBOSE
    Serial.println("Sensor max value calibration starts in 5 seconds. Please clear the front of sensor at least 5 cm distance.");
  
    for(i = 5; i > 0; i--){
      Serial.print("Last ");
      Serial.print(i);
      Serial.println(" second(s)");
      delay(999);
    }
    Serial.println("");
  
  #else
    delay(3000);

  #endif

  // Get Clear Color Envirement Values for max RGB
  get_color(s2, s3, sensor_out, rgb_color, true);
  
  #if IS_VERBOSE
    Serial.print("Maximum Envirement Value (Black or No Color): ");
    Serial.print(rgb_color[0]);
    Serial.print(" | ");
    Serial.print(rgb_color[1]);
    Serial.print(" | ");
    Serial.println(rgb_color[2]);
  #endif
  
  // Get Average of R, G, B values
  // Set the giveen destinition array - Note: Added 60 value because of getting more stable result
  min_max_rgb[1] = (rgb_color[0] + rgb_color[1] + rgb_color[2] + 60 ) / 3;
  
  #if IS_VERBOSE
    Serial.print("Mapped Maximum Envirement Value (Black or No Color): ");
    Serial.print(rgb_color[0]);
    Serial.print(" | ");
    Serial.print(rgb_color[1]);
    Serial.print(" | ");
    Serial.println(rgb_color[2]);
  #endif
  
}


void auto_config_color_sensor(int s0, int s1, int s2, int s3, int sensor_out, cs_freq_scale scale) {
  
  #if IS_VERBOSE
    Serial.print("Enabling Color Sensor on ");
    Serial.print(" | S0: ");
    Serial.print(s0);
    Serial.print(" | S1: ");
    Serial.print(s1);
    Serial.print(" | S2: ");
    Serial.print(s2);
    Serial.print(" | S3: ");
    Serial.print(s3);
    Serial.print(" | Sensor Out: ");
    Serial.print(sensor_out);
    Serial.println(" PINs");
  #endif
  
  pinMode(s0, OUTPUT);
  pinMode(s1, OUTPUT);
  pinMode(s2, OUTPUT);
  pinMode(s3, OUTPUT);
  pinMode(sensor_out, INPUT);
  
  frequency_scale_config_color_sensor(scale);

  #if IS_VERBOSE
    Serial.println("Enabled Color Sensor");
  #endif
}


void auto_config_ir_remote(int ir_reciever_pin) {
  
  #if IS_VERBOSE
    Serial.print("Enabling IRin on ");
    Serial.print(ir_reciever_pin);
    Serial.println(" PIN");
  #endif
  
  ir_reciever_decoder.enableIRIn();
  ir_reciever_decoder.blink13(true);

  #if IS_VERBOSE
    Serial.println("Enabled IRin");
  #endif
}


void auto_config_rgb_led(int red_pin, int green_pin, int blue_pin){
  
  #if IS_VERBOSE
    Serial.print("Enabling RGB LED on ");
    Serial.print(" | R: ");
    Serial.print(red_pin);
    Serial.print(" | G: ");
    Serial.print(green_pin);
    Serial.print(" | B: ");
    Serial.print(blue_pin);
    Serial.println(" PINs");
  #endif
  
  pinMode(red_pin, OUTPUT);
  pinMode(green_pin, OUTPUT);
  pinMode(blue_pin, OUTPUT);
  
  #if IS_VERBOSE
    Serial.println("Enabled RGB LED");
  #endif
}


void frequency_scale_config_color_sensor(cs_freq_scale scale){
  /*
    | S0 - S1 | = Output Frequency Scaling
    -____|____-
    | L  - L  | = Power Down
    | L  - H  | = %2 
    | H  - L  | = %20 (Optimal for Arduino)
    | H  - H  | = %100
  */
  
  if(scale == low_low){
    // Setting frequency-scaling to 0% - Power Down
    digitalWrite(S0, LOW);
    digitalWrite(S1, LOW);
  }
  else if(scale == low_high){
    // Setting frequency-scaling to 2%
    digitalWrite(S0, LOW);
    digitalWrite(S1, HIGH);    
  }
  else if(scale == high_low){
    // Setting frequency-scaling to 20%
    digitalWrite(S0, HIGH);
    digitalWrite(S1, LOW);
  }
  else if(scale == high_high){
    // Setting frequency-scaling to 100%
    digitalWrite(S0, HIGH);
    digitalWrite(S1, HIGH);
  }
  
}


void rgb_led_and_color_sensor_calibration(int * rgb_led_pin, int s2, int s3, int sensor_out, int learn_time){
  
  int current_light_average;
  int counter = 0;
  int rgb_color[3] = {0, 0, 0};
  
  
  #if IS_VERBOSE
    Serial.println("Color Sensor Calibration with RGB LED starting. Please wait... ");
    int i;
    for(i = 3; i > 0; i--){
      Serial.print("Last ");
      Serial.print(i);
      Serial.println(" second(s)");
      delay(999);
    }
    Serial.println("");
  #else
    delay(3000);
  #endif
  
  
  // Get an envirement value for start values of minimum and maximum
  get_color(s2, s3, sensor_out, rgb_color, true);
  min_max_rgb[0] = ( rgb_color[0] + rgb_color[1] + rgb_color[2] ) / 3;
  min_max_rgb[1] = min_max_rgb[0];
  
  // White - Minimum
  rgb_color_switch(white, rgb_led_pin);
  
  for(counter = learn_time; counter > 0; counter--){
    #if IS_VERBOSE
      Serial.print(".");
    #endif
    get_color(s2, s3, sensor_out, rgb_color, true);
    current_light_average = ( rgb_color[0] + rgb_color[1] + rgb_color[2] ) / 3;
    if(current_light_average < min_max_rgb[0]){
      min_max_rgb[0] = current_light_average;
    }
  }
  
  #if IS_VERBOSE
    Serial.println("");
  #endif
  
  // Black - Maximum
  rgb_color_switch(black, rgb_led_pin);
  
  for(counter = learn_time; counter > 0; counter--){
    #if IS_VERBOSE
      Serial.print(".");
    #endif
    get_color(s2, s3, sensor_out, rgb_color, true);
    current_light_average = ( rgb_color[0] + rgb_color[1] + rgb_color[2] ) / 3;
    if(current_light_average > min_max_rgb[1]){
      min_max_rgb[1] = current_light_average;
    }
  }
  
  
  #if IS_VERBOSE
    Serial.println("");
    Serial.print("Envirement Learn Finished. Minimum Value: ");
    Serial.print(min_max_rgb[0]);
    Serial.print(" | Maximum Value: ");
    Serial.println(min_max_rgb[1]);
    Serial.println("Color Recognition Starting... ");
  #endif
  for(int i = 2; i > 0; i--){
    rgb_color_switch(blue_normal, rgb_led_pin);
    delay(100);
    rgb_color_switch(green_normal, rgb_led_pin);
    delay(100);
    rgb_color_switch(red_normal, rgb_led_pin);
    delay(100);
  }
  /*
  // Red Configuration
  rgb_color_switch(red_normal, rgb_led_pin);
  get_color(S2, S3, SENSOR_OUT, rgb_readed_color, false)
  
  
  // Green Configuration
  rgb_color_switch(green_normal, rgb_led_pin);
  
  
  // Blue Configuration
  rgb_color_switch(blue_normal, rgb_led_pin);
  */
}


// ==================
// BUILT-IN FUNCTIONS
// ==================

void setup(void) {
  #if IS_VERBOSE
    Serial.begin(9600);
    Serial.println("\n\nSTART\n\n");
  #endif
  
  auto_config_color_sensor(S0, S1, S2, S3, SENSOR_OUT, high_low);
  auto_config_ir_remote(IR_RECIEVER);
  auto_config_rgb_led(RED_PIN, GREEN_PIN, BLUE_PIN);

  #if LEARN
    rgb_led_and_color_sensor_calibration(rgb_led_pins, S2, S3, SENSOR_OUT, ENVIREMENT_LEARN_TIME_SECONDS);
  #else
    calibrate_color_sensor(S2, S3, SENSOR_OUT, rgb, min_max_rgb);
  #endif
}


void loop(void) {
  
  smart_color_desicion(RENDER_TIME, true);
  
  action_ir_remote(rgb_led_pins, rgb_led_color);
  
}
